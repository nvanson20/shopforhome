package com.hcl.capstone;

import com.hcl.capstone.service.FileUploadService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


import javax.annotation.Resource;

@SpringBootApplication
@EnableEurekaClient
public class CapstoneProductApplication implements CommandLineRunner {
    @Resource
    FileUploadService fileUploadService;

    public static void main(String[] args) {
        SpringApplication.run(CapstoneProductApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        fileUploadService.loadAll();
    }
}
