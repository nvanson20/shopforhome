package com.hcl.capstone.service;

import com.hcl.capstone.model.dto.request.ProductRequest;
import com.hcl.capstone.model.entity.Product;
import com.hcl.capstone.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(Integer id) {
        return productRepository.findProductById(id);
    }

    public void addNewProduct(Product product) {
        productRepository.save(product);
    }

    public void updateProductQuantity(Integer productId, Integer quantity) {
        Product product = getProductById(productId);
        product.setStock(product.getStock() - quantity);
        productRepository.save(product);
    }

    public void updateProduct(Integer productId, ProductRequest productRequest) {
        Product product = getProductById(productId);
        product.setName(productRequest.getName());
        product.setPrice(productRequest.getPrice());
        product.setStock(productRequest.getStock());
        productRepository.save(product);
    }

    public void deleteProductById(Integer id) {
        productRepository.deleteById(id);
    }

}
