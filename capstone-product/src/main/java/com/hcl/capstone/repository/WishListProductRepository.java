package com.hcl.capstone.repository;

import com.hcl.capstone.model.entity.WishList;
import com.hcl.capstone.model.entity.WishListProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WishListProductRepository extends JpaRepository<WishListProduct, Integer> {

    Boolean existsByProductId(Integer productId);

    List<WishListProduct> findWishListProductsByWishList(WishList wishList);

    void deleteWishListProductByProductId(Integer productId);
}
