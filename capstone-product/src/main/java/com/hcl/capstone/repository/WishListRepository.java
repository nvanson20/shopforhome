package com.hcl.capstone.repository;

import com.hcl.capstone.model.entity.WishList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WishListRepository extends JpaRepository<WishList, Integer> {
    WishList findWishListByUserId(Integer userId);
}
