package com.hcl.capstone.controller;

import com.hcl.capstone.model.entity.User;
import com.hcl.capstone.model.dto.request.UserRequest;
import com.hcl.capstone.model.dto.response.MessageResponse;
import com.hcl.capstone.model.dto.response.UserResponse;
import com.hcl.capstone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@RestController
@CrossOrigin(value = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private WebClient.Builder webClientBuilder;

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable Integer id) {
        UserResponse user = userService.getUserById(id);
        return ResponseEntity.ok(user);
    }

    @PatchMapping("/update-user/{id}")
    public ResponseEntity<Object> updateUserProfile(@RequestBody UserRequest user, @PathVariable Integer id) {
        userService.updateUserProfile(user, id);
        return ResponseEntity.ok(new MessageResponse("Update customer successfully"));
    }

    @DeleteMapping("/delete-user/{id}")
    public ResponseEntity<Object> deleteUserAccount(@PathVariable Integer id) {
        userService.deleteUserById(id);
        return ResponseEntity.ok(new MessageResponse("Delete user id " + id + " successfully"));
    }

    @GetMapping("/admin/list-customers")
    public ResponseEntity<Object> getAllCustomers() {
        List<User> users = userService.getAllCustomers();
        return ResponseEntity.ok(users);
    }
}
