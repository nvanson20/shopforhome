package com.hcl.capstone.model.dto.response;

import com.hcl.capstone.model.entity.ERole;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserResponse {
    private Integer id;
    private String username;
    private String email;
    private String phone;
    private String address;
    private String city;
    private Integer wishListId;
    private ERole role;
    private String token;
}
